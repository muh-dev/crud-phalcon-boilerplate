<style>
    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
    }
</style>

{{ form('/', 'role': 'form', 'class': 'form-signin') }}
    <h4 class="h4 mb-4 font-weight-normal">Silahkan Input Data</h4>
    <label for="name" class="sr-only">Nama</label>
    <input type="text" name="txt_nama" class="form-control" placeholder="Nama Anda" autofocus>
    
    <label for="email" class="sr-only">Email</label>
    <input type="email" name="txt_email" class="form-control" placeholder="Email Anda">

    <label for="phone" class="sr-only">Phone</label>
    <input type="text" name="txt_phone" class="form-control" placeholder="Phone Anda">

    <label for="address" class="sr-only">Address</label>
    <textarea name="txt_address" id="txt_address" cols="30" rows="10" placeholder="Address Anda"></textarea>

    <button class="btn btn-lg btn-primary btn-block btnSave" type="submit">Save</button>
    
    <div id="viewdata">
        <?= $this->tag->linkTo(['viewdata', 'Lihat Data!','class' => 'buttonLink']) ?>
    </div>
</form>